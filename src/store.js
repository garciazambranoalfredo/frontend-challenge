import { configureStore } from "@reduxjs/toolkit";
import endpointReducer from "./slices/endpoint/endpointSlice";

export default configureStore({
  reducer: {
    endpoint: endpointReducer,
  },
});
