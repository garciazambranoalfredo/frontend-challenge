import { createSlice } from "@reduxjs/toolkit";

const ORDER_ASC = "orderBy=onsaleDate";
const ORDER_DES = "orderBy=-onsaleDate";

const initialState = {
  value: "comics",
  order: "ASC",
  orderParam: ORDER_ASC,
  byTitleParam: "",
  creatorName: "",
};

const endpointSlice = createSlice({
  name: "endpoint",
  initialState,
  reducers: {
    setOrder(state) {
      if (state.order === "ASC") {
        state.orderParam = ORDER_DES;
        state.order = "DES";
      } else {
        state.orderParam = ORDER_ASC;
        state.order = "ASC";
      }
    },
    setByCreator(state, action) {
      state.value = action.payload.endpoint;
      state.creatorName = action.payload.name;
    },
    resetByCreator(state) {
      state.value = "comics";
      state.creatorName = "";
    },
    filterByTitle(state, action) {
      if (action.payload !== "") {
        state.byTitleParam = action.payload;
      } else {
        state.byTitleParam = "";
      }
    },
    resetFilterByTitle(state) {
      state.byTitleParam = "";
    },
  },
});

export const {
  setOrder,
  setByCreator,
  filterByTitle,
  resetByCreator,
  resetFilterByTitle,
} = endpointSlice.actions;

export default endpointSlice.reducer;
