import "./styles/bootstrap.min.css";
import Navbar from "./components/Navbar";
import Feed from "./components/Feed";

function App() {
  return (
    <div className="App">
      <Navbar />
      <Feed />
    </div>
  );
}

export default App;
