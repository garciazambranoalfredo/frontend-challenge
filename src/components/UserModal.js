import React, { useEffect } from "react";
import { Modal, Button } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";
import { setShow } from "../slices/modal/modalSlice";

function UserModal() {
  const user = useSelector((state) => state.modal.user);
  const show = useSelector((state) => state.modal.value);
  const dispatch = useDispatch();

  return (
    user !== null && (
      <Modal
        show={show}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header>
          <Modal.Title id="contained-modal-title-vcenter">
            {`${user.firstName} ${user.lastName}`}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="d-flex justify-content-center align-items-center">
            <img className="rounded me-4" src={user.picture} alt="user" />
            <a
              href={`mailto:${user.email}`}
              target="_blank"
              rel="noopener noreferrer"
            >
              {user.email}
            </a>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => dispatch(setShow(false))}>Close</Button>
        </Modal.Footer>
      </Modal>
    )
  );
}

export default UserModal;
