import React from "react";
import Logo from "../images/logo.png";
import { NavbarImage } from "./Styled";
function Navbar() {
  return (
    <div className="bg-light shadow position-sticky">
      <div className="navbar d-flex justify-content-center">
        <NavbarImage src={Logo} alt="team pets logo" />
      </div>
    </div>
  );
}

export default Navbar;
