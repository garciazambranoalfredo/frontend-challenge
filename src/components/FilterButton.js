import React from "react";
import { useDispatch } from "react-redux";
import {
  resetFilterByTitle,
  resetByCreator,
} from "../slices/endpoint/endpointSlice";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";

function FilterButton({ value, tag }) {
  const dispatch = useDispatch();
  function handleClick() {
    if (tag === "creator") {
      dispatch(resetByCreator());
    } else if (tag === "title") {
      dispatch(resetFilterByTitle());
    }
  }
  return (
    <div className="bg-primary rounded py-1 px-2 me-4">
      <span className="fw-bold text-light">{value}</span>
      <button className="btn btn-primary ms-2" onClick={() => handleClick()}>
        <FontAwesomeIcon icon={faTimes} />
      </button>
    </div>
  );
}

export default FilterButton;
