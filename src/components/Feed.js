import React, { useState, useEffect } from "react";
import axios from "axios";
import SearchBar from "./SearchBar";
import Card from "./Card";
import CreatorsList from "./CreatorsList";
import FilterButton from "./FilterButton";
import ReactPaginate from "react-paginate";
import { PaginationContainer } from "./Styled";
import { useSelector } from "react-redux";
const BASE_URL = "http://gateway.marvel.com/v1/public/";
const TS = "ts=1";
const API_KEY = "apikey=258159386a6b2926cdd666ca4dc065e5"; //73996d34d76fef7916a79cc626174f6d
const HASH = "hash=a808e3a90ff0aa824bdcb90c24ab7cba"; //0cb20d8d88bc5a17937782346e44029b
const limit = 10;

function Feed() {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [offset, setOffset] = useState(0);
  const [pageCount, setPageCount] = useState(0);

  function handlePageClick(e) {
    setOffset(limit * e.selected);
  }

  const endpointRoot = useSelector((state) => state.endpoint.value);
  const orderParam = useSelector((state) => state.endpoint.orderParam);
  const titleParam = useSelector((state) => state.endpoint.byTitleParam);
  const creatorName = useSelector((state) => state.endpoint.creatorName);

  useEffect(() => {
    let offsetParam = `offset=${offset}`;
    setLoading(true);
    window.scrollTo(0, 0);

    let query =
      titleParam !== ""
        ? `${BASE_URL}${endpointRoot}?${orderParam}&limit=${limit}&${offsetParam}&titleStartsWith=${titleParam}&${TS}&${API_KEY}&${HASH}`
        : `${BASE_URL}${endpointRoot}?${orderParam}&limit=${limit}&${offsetParam}&${TS}&${API_KEY}&${HASH}`;

    axios
      .get(query)
      .then((response) => {
        setData(response.data.data.results);
        setPageCount(response.data.data.total / limit);
      })
      .catch(console.error)
      .finally(() => setLoading(false));
  }, [endpointRoot, orderParam, titleParam, offset]);

  return (
    <>
      <div className="d-flex align-items-center flex-column col-10 col-md-8 container-xl">
        <SearchBar />

        <div className="d-flex col-12">
          {titleParam !== "" && <FilterButton value={titleParam} tag="title" />}

          {creatorName !== "" && (
            <FilterButton value={creatorName} tag="creator" />
          )}
        </div>

        <div className="d-flex col-12">
          <div className="col-md-8">
            {loading ? (
              <div className="my-4 col-12 col-md-10">
                <h4>Loading...</h4>
              </div>
            ) : data.length > 1 ? (
              data.map((post) => (
                <Card
                  title={post.title}
                  cover={post.thumbnail}
                  creators={post.creators.items}
                  date={post.dates}
                  key={post.id}
                />
              ))
            ) : (
              <div className="my-4 col-12 col-md-10">
                <h4>Any item found, try again.</h4>
              </div>
            )}
          </div>
          <CreatorsList />
        </div>
      </div>

      <PaginationContainer className="my-4 mx-auto">
        <ReactPaginate
          previousLabel={"<<"}
          nextLabel={">>"}
          breakLabel={"..."}
          pageCount={pageCount}
          pageClassName={"page-item"}
          pageLinkClassName={"page-link"}
          previousClassName={"page-item"}
          previousLinkClassName={"page-link"}
          nextClassName={"page-item"}
          nextLinkClassName={"page-link"}
          marginPagesDisplayed={1}
          pageRangeDisplayed={1}
          onPageChange={(e) => handlePageClick(e)}
          containerClassName={"pagination"}
          activeClassName={"active"}
        />
      </PaginationContainer>
    </>
  );
}

export default Feed;
