import React from "react";
import Moment from "react-moment";
import { UserLink, CreatorsContainer } from "./Styled";
import { useDispatch } from "react-redux";
import { setByCreator } from "../slices/endpoint/endpointSlice";

function Card({ title, cover, creators, date }) {
  const dispatch = useDispatch();

  function filterCreator(creator) {
    //This will return the endpoint to filter by creator
    let creatorEndpoint = `${creator.resourceURI.slice(
      creator.resourceURI.indexOf("creators")
    )}/comics`;
    let creatorName = creator.name;
    return { endpoint: creatorEndpoint, name: creatorName };
  }

  return (
    <div className="shadow card my-4 col-12 col-md-10">
      <img
        src={`${cover.path}.${cover.extension}`}
        className="card-img-top"
        alt="comic cover"
      />
      <div className="card-body">
        <Moment format="YYYY/MM/DD">{date[0].date}</Moment>
        <h5 className="card-title mt-2">{title}</h5>
        <CreatorsContainer className="d-flex my-2">
          <p className="card-text me-2">Creators:</p>
          {creators.map((creator) => (
            <UserLink
              key={creator.name}
              className="card-text ms-3"
              onClick={() => {
                dispatch(setByCreator(filterCreator(creator)));
              }}
            >
              {creator.name}
            </UserLink>
          ))}
        </CreatorsContainer>
      </div>
    </div>
  );
}

export default Card;
