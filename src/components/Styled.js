import styled from "styled-components";

export const UserLink = styled.p`
  cursor: pointer;

  &:hover {
    color: #0d6efd;
  }
`;

export const NavbarImage = styled.img`
  max-height: 3.5rem;
`;

export const CreatorsContainer = styled.div`
  overflow-x: scroll
`;

export const PaginationContainer = styled.div`
  width: fit-content;
`;
