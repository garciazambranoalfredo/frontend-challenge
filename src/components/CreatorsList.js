import React, { useEffect, useState } from "react";
import { UserLink } from "./Styled";
import axios from "axios";
import { useDispatch } from "react-redux";
import { setByCreator } from "../slices/endpoint/endpointSlice";

const BASE_URL = "http://gateway.marvel.com/v1/public/creators";
const TS = "ts=1";
const API_KEY = "apikey=73996d34d76fef7916a79cc626174f6d"; //258159386a6b2926cdd666ca4dc065e5
const HASH = "hash=0cb20d8d88bc5a17937782346e44029b"; //a808e3a90ff0aa824bdcb90c24ab7cba
const orderParam = "orderBy=lastName";
const limit = 20;

function filterCreator(creator) {
  //This will return the endpoint to filter by creator
  let creatorEndpoint = `creators/${creator.id}/comics`;
  let creatorName = creator.fullName;
  return { endpoint: creatorEndpoint, name: creatorName };
}

function CreatorList() {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const dispatch = useDispatch();

  useEffect(() => {
    axios
      .get(`${BASE_URL}?${orderParam}&limit=${limit}&${TS}&${API_KEY}&${HASH}`)
      .then((response) => {
        setData(response.data.data.results);
      })
      .catch(console.error)
      .finally(() => setLoading(false));
  });

  return (
    <div className="d-none d-md-flex flex-column align-items-center">
      <h4 className="mt-4">CREATORS</h4>
      <ul className="list-group position-sticky mt-2">
        {loading ? (
          <div className="col-12 col-md-10">
            <h4>Loading...</h4>
          </div>
        ) : (
          data.map((creator) => (
            <li
              className="list-group-item card-text user-link"
              onClick={() => {
                dispatch(setByCreator(filterCreator(creator)));
              }}
            >
              <UserLink>{creator.fullName}</UserLink>
            </li>
          ))
        )}
      </ul>
    </div>
  );
}

export default CreatorList;
