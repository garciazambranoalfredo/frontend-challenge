import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faSearch,
  faSortUp,
  faSortDown,
} from "@fortawesome/free-solid-svg-icons";
import { useDispatch, useSelector } from "react-redux";
import { filterByTitle, setOrder } from "../slices/endpoint/endpointSlice";

function SearchBar() {
  const [title, setTitle] = useState("");
  const dispatch = useDispatch();
  const order = useSelector((state) => state.endpoint.order);

  function handleSubmit(e) {
    e.preventDefault();
    dispatch(filterByTitle(title));
    setTitle("");
  }

  return (
    <form onSubmit={(e) => handleSubmit(e)} className="row g-3 mx-auto my-4">
      <div className="col-auto">
        <label htmlFor="input-title" className="visually-hidden">
          Password
        </label>
        <input
          type="text"
          className="form-control"
          id="input-title"
          placeholder="Search by title"
          value={title}
          onChange={(e) => setTitle(e.target.value)}
        />
      </div>
      <div className="col-auto">
        <button type="submit" className="btn btn-primary mb-3">
          <FontAwesomeIcon icon={faSearch} />
        </button>
      </div>
      <div className="col-auto">
        <button
          className="btn btn-primary mb-3"
          onClick={() => dispatch(setOrder())}
        >
          {order === "ASC" ? (
            <FontAwesomeIcon icon={faSortDown} />
          ) : (
            <FontAwesomeIcon icon={faSortUp} />
          )}
        </button>
      </div>
    </form>
  );
}

export default SearchBar;
